<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- TITLE -->
    <title>U Zapały - Dane Kontaktowe</title>
    <!-- CSS -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
        <!-- JS -->
        <script src="../assets/app/observes.js" defer></script>
        <script src="../assets/app/mobile-navbar.js" defer></script>
        <script src="../assets/app/icons.js" defer></script>
</head>
<body>

    <!-- Navigation -->
    <header>
        <div class="nav-default">
            <a href="../index.html"><img src="../assets/img/logo.png" alt="Logo u-zapaly.pl"></a>
            <button class="burger">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </button>
        </div>
        <ul class="nav-list">
            <a href="../index.html" class="nav-list__element"><div class="nav-list__element__background"><li>HOME</li></div></a>
            <a href="gallery.html" class="nav-list__element"><div class="nav-list__element__background"><li>GALERIA</li></div></a>
            <a href="around.html" class="nav-list__element"><div class="nav-list__element__background"><li>OKOLICE</li></div></a>
            <a href="prices.html" class="nav-list__element"><div class="nav-list__element__background"><li>CENNIK</li></div></a>
            <a href="contact.php" class="nav-list__element current"><div class="nav-list__element__background"><li>KONTAKT</li></div></a>
        </ul>
        <section class="nav-social">
            <a href="https://www.facebook.com/pokojeuzapaly/" target="_blank">
                <div class="nav-social__buttons button-facebook">
                    <i class="fa-brands fa-facebook-f"></i>
                    <span class="tooltiptext tooltip-facebook">Facebook</span>
                </div>
            </a>
            <a href="https://www.booking.com/reviews/pl/hotel/u-zapaly.pl.html" target="_blank">
                <div class="nav-social__buttons button-booking">
                    <i class="fa-solid fa-b">.</i>
                    <span class="tooltiptext tooltip-booking">Booking</span>
                </div>
            </a>
            <a href="../path/contact.php">
                <div class="nav-social__buttons button-call">
                    <i class="fa-solid fa-phone"></i>
                    <span class="tooltiptext tooltip-call">Zadzwoń</span>
                </div>
            </a>
        </section>
    </header>
    <div class="overlay"></div>
    
    <!-- Main -->
    <main>

<!-- ---------------------------------------------------- -->


        <!-- Background -->
        
        <!-- Background Image -->
        
        <section class="background-header contact-home">
            <img class="contact-background contact-page" src="../assets/img/kontakt-jpg.jpg" alt="">
            <div class="subpage-header">
                <h1>Kontakt</h1>
                <p>Lokalizacja i Informacje</p>
            </div>
            <div class="contact-content">
                <div class="contact-option">
                    <div><i class="fa-solid fa-location-dot"></i>Adres</div> 
                    <hr class="underline">
                    <p>Poręba Wielka 182, <br />
                        34-735 Niedźwiedź</a>
                </div>
                <div class="contact-option">
                    <div><i class="fa-solid fa-phone"></i>Kontakt</div>
                    <hr class="underline">
                    <a href="mailto:u-zapaly@wp.pl">u-zapaly@wp.pl</a>
                    <a href="tel:502 521 004">502 521 004</a>
                </div>
            </div>
        </section>
        <!-- Google Maps -->

        <section class="location">
            <h1 class="heading">Sprawdź jak dojechać:</h1>
            <hr class="underline">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d161.61155076381218!2d20.069563439670763!3d49.60180730525144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471609c45a4aa4f9%3A0x7f3bbb8edaa031fe!2sGospodarstwo%20Agroturystyczne%20U%20Zapa%C5%82y!5e0!3m2!1spl!2spl!4v1662407821715!5m2!1spl!2spl" 
                    width="100%" 
                    height="500" 
                    style="border:0;" 
                    allowfullscreen="" 
                    loading="lazy" 
                    referrerpolicy="no-referrer-when-downgrade">
            </iframe>
        </section>

        <!-- Send E-Mail -->

        <section class="sendmail">
            <h1 class="heading"><i class="fa-solid fa-envelope"></i>Wyślij wiadomość: </h1>
            <hr class="underline">
            <form action="" method="POST" class="sendmail__input fade-in">
                <label for="email">Adres e-mail:</label>
                <input id="email" name="email" type="email" placeholder="Wpisz adres..." autocomplete="off" required>
                <label for="title">Tytuł:</label>
                <input id="title" name="title" type="text" placeholder="Podaj tytuł..." autocomplete="off" required>
                <label for="msg">Treść wiadomości:</label>
                <textarea id="msg" name="msg" placeholder="Wpisz wiadomość..." autocomplete="off" cols="30" rows="10" required></textarea>
                <button class="sendmail__button" type="submit" id="mailBtn" name="mailBtn">Wyślij</button>
            </form> 
        </section>
<!-- ---------------------------------------------------- -->
        <?php     
            if(isset($_POST['mailBtn'])) 
            {
                $to         = "bartek3xe@gmail.com";
                $subject    = wordwrap($_POST['title'], 70);
                $body       = $_POST['msg'];
                $header     = $_POST['email'];

                mail($to, $subject, $body, $header);
            }
        ?>
<!-- ---------------------------------------------------- --> 
        <footer>
            <section class="footer__container">
                <div class="footer--info">
                    <img src="../assets/img/logo.png" alt="logo U-Zapały w stopce">
                    <div class="links">
                        <a href="" class="footer-location"><i class="fa-solid fa-location-dot"></i></a>
                        <a href="" class="footer-booking"><i class="fa-solid fa-b">.</i></a>
                        <a href="" class="footer-facebook"><i class="fa-brands fa-facebook-f"></i></a>
                    </div>
                </div>
                <div class="footer--con">
                    <h1 class="footer-header">SKONTAKTUJ SIĘ Z NAMI!</h1>
                    Koninki 182, <br /> 
                    34-735 Niedźwiedź <br /><br />
                    <a href="tel:502 521 004"><i class="fa-solid fa-phone"></i>502 521 004</a>
                    <a href="mailto:u-zapaly@wp.pl"><i class="fa-regular fa-envelope"></i>u-zapaly@wp.pl</a>
                </div>
                <div class="footer--shortcut">
                    <h1 class="footer-header">NA SKRÓTY...</h1>
                    <ul>
                        <li><a href="../index.html">HOME</a></li>
                        <li><a href="gallery.html">GALERIA</a></li>
                        <li><a href="around.html">OKOLICE</a></li>
                        <li><a href="prices.html">CENNIK</a></li>
                        <li><a href="contact.php">KONTAKT</a></li>
                    </ul>
                </div>
            </section>
            <div class="footer--copyright">
                U-Zapały 2022 <i class="fa fa-copyright"></i>
        </div>
        </footer>
    </main>
</body>
</html>