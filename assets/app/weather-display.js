const button = document.querySelector('.weather-button');
const weather = document.querySelector('.weather');
const arrow = document.querySelector('.arrow');

button.onclick = () => {
    weather.classList.toggle('weather-active');
    arrow.classList.toggle('rotate-arrow');
};

