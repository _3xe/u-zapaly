const mobileNav = document.querySelector('.nav-list');
const burgerIcon = document.querySelector('.burger');
const overlay = document.querySelector('.overlay');

function burgerOutline () {
    addEventListener('blur', () => {
        burgerIcon.style.outline="1px solid white;"; 
    });
};

function expandFromBurger () {
    burgerIcon.classList.toggle('active');
    overlay.classList.toggle('active');
    mobileNav.classList.toggle('active');
};

function expandFromOverlay () {
    burgerIcon.classList.toggle('active');
    overlay.classList.toggle('active');
    mobileNav.classList.toggle('active');
}

burgerOutline();
burgerIcon.addEventListener('click', expandFromBurger);
overlay.addEventListener('click', expandFromOverlay);