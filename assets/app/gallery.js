

const selectedImage = document.querySelectorAll('.image img'); 
const spanOverlay = document.querySelector('.popup-image span');

const leftArrow = document.querySelector('.left-arrow');
const rightArrow = document.querySelector('.right-arrow');

let focusOnImage = document.querySelectorAll('.image');

let currentImageIndex;

const onGallery = () => {
    selectedImage.forEach((image, index) =>{
        image.addEventListener('click', () =>{
            document.querySelector('.popup-image').classList.remove('hidden');
            document.querySelector('.popup-image img').src = image.getAttribute('src');
            currentImageIndex = index;
        });
    });
};

const onGalleryByEnter = () => {
    selectedImage.forEach((image, index) => {
        if(document.activeElement === image){
            document.querySelector('.popup-image').classList.remove('hidden');
            document.querySelector('.popup-image img').src = image.getAttribute('src');
            currentImageIndex = index;
        }
    })
}

const offGallery = () => {
    document.querySelector('.popup-image').classList.add('popup-fadeout');
    setTimeout(() => {
        document.querySelector('.popup-image').classList.add('hidden');
        document.querySelector('.popup-image').classList.remove('popup-fadeout');
    }, 300);        
};
    

const showNextImage = () => {
    if(currentImageIndex === selectedImage.length - 1) {
        currentImageIndex = 0;
    } else {
        currentImageIndex++;
    }
    document.querySelector('.popup-image img').src = selectedImage[currentImageIndex].src;
};


const showPrevImage = () => {
    if(currentImageIndex === 0) {
        currentImageIndex = selectedImage.length - 1;
    } else {
        currentImageIndex--;
    }
    document.querySelector('.popup-image img').src = selectedImage[currentImageIndex].src;
};



onGallery();
spanOverlay.addEventListener('click', offGallery);
rightArrow.addEventListener('click', showNextImage);
leftArrow.addEventListener('click', showPrevImage);

document.addEventListener('keydown', (e) => {
    if(e.key === "ArrowRight") {
        showNextImage();
    };
});

document.addEventListener('keydown', (e) => {
    if(e.key === "ArrowLeft") {
        showPrevImage();
    };
});

document.addEventListener('keydown', (e) => {
    if(e.key === "Escape") {
       offGallery();
    };
});

document.addEventListener('keydown', (e) => {
    if(e.key === "Enter") {
        onGalleryByEnter();
    };
});

